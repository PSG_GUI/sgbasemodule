//
//  SGFirstViewController.h
//  SGBaseModule_Example
//
//  Created by SG on 2019/8/31.
//  Copyright © 2019 SG. All rights reserved.
//

#import <SGBaseModule/SGViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SGFirstViewController : SGViewController

@end

NS_ASSUME_NONNULL_END
